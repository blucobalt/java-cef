import org.gradle.internal.os.OperatingSystem

plugins {
    java
}

group = "org.cef"
version = "1.0-SNAPSHOT"

// build.xml's targets share names with gradle's so we prefix them with ant- to prevent breakage
// eg ant's "compile" task becomes "ant-compile" in gradle and "bundle" becomes "ant-bundle" in gradle
ant.importBuild("build.xml") { antTargetName ->
    "ant-$antTargetName"
}

dependencies {
    implementation(files("$projectDir/third_party/jogamp/jar/gluegen-rt.jar"))
    implementation(files("$projectDir/third_party/jogamp/jar/gluegen-rt-natives-macosx-universal.jar"))
    implementation(files("$projectDir/third_party/jogamp/jar/jogl-all.jar"))
    implementation(files("$projectDir/third_party/jogamp/jar/jogl-native-macosx-universal.jar"))
    implementation(files("$projectDir/third_party/junit/junit-platform-console-standalone-1.4.2.jar"))
}

tasks.compileJava {
    // ensures jcef is compiled to java 8 compatible bytecode
    options.release.set(8)
}

sourceSets {
    main {
        java {
            // exclude the mac package if we are not on mac
            if (!OperatingSystem.current().isMacOsX) {
                exclude("org/cef/browser/mac/CefBrowserWindowMac.java")
            }
        }
    }
}
