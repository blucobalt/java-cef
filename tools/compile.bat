@echo off
:: Copyright (c) 2013 The Chromium Embedded Framework Authors. All rights
:: reserved. Use of this source code is governed by a BSD-style license
:: that can be found in the LICENSE file.

set RETURNCODE=
setlocal
cd ..

if "%1" == "" (
echo ERROR: Please specify a target platform: win32 or win64
set ERRORLEVEL=1
goto end
)

set OUT_PATH=".\out\%1"
set CLS_PATH=".\third_party\jogamp\jar\*;.\third_party\junit\*;.\java"

if not exist %OUT_PATH% mkdir %OUT_PATH%
javac -Xdiags:verbose -cp %CLS_PATH% -d %OUT_PATH% src/test/java/tests/detailed/*.java src/test/java/tests/detailed/dialog/*.java src/test/java/tests/detailed/handler/*.java src/test/java/tests/detailed/ui/*.java src/test/java/tests/detailed/util/*.java src/test/java/tests/junittests/*.java src/test/java/tests/simple/*.java src/main/java/org/cef/*.java src/main/java/org/cef/browser/*.java src/main/java/org/cef/callback/*.java src/main/java/org/cef/handler/*.java src/main/java/org/cef/misc/*.java src/main/java/org/cef/network/*.java

:: Copy MANIFEST.MF
xcopy /sfy .\src\main\resources\META-INF\ %OUT_PATH%\manifest\

:: Copy resource files.
xcopy /sfy .\src\test\resources\*.html %OUT_PATH%\tests\detailed\handler\
xcopy /sfy .\src\test\resources\*.png %OUT_PATH%\tests\detailed\handler\

:end
endlocal & set RETURNCODE=%ERRORLEVEL%
goto omega

:returncode
exit /B %RETURNCODE%

:omega
call :returncode %RETURNCODE%
